/* 
 * File:   main.cpp
 */

#include <iostream>
#include <fstream>

#include <boost/program_options.hpp>
#include <boost/format.hpp>
//#include <boost/algorithm/string.hpp>
//#include <boost/algorithm/string/split.hpp>

#include "SxGeoParser.h"

using namespace std;

namespace po = boost::program_options;

struct Params_t
{
  bool is_showDbInfo;
  string dbFileName;
  string addrsFileName;
  string geoFileName;
};

bool program_options_handler(Params_t * params, int argc, char** argv);

int main(int argc, char** argv)
{
  Params_t params;
  if (program_options_handler(&params, argc, argv)) return 0;
  
  cout << endl
    << "SxGeoParser unit test" << endl
    << " - db file: \"" << params.dbFileName << "\"" << endl
    << " - addrs file: \"" << params.addrsFileName << "\"" << endl
    << " - output file: \"" << params.geoFileName << "\"" << endl
    << endl
  ;
  
  SxGeoParser_t parser(params.dbFileName);
  if (parser.isError())
  {
    cout << "parser error: " << parser.errorString() << endl;
  }
  
  if (params.is_showDbInfo)
  {
    SxGeoParser_t::dbHeader_t dbHeader = parser.getDbHeader();
    if (!dbHeader.isValid)
    {
      cout << "DB header not valid" << endl;
      return -1;
    }
    
    cout << endl
      << "DB Info:" << endl
      << " - version: " << parser.dbVersionStr() << endl
      << " - type: " << parser.dbTypeStr() << endl
      << " - encoding: " << parser.dbEncodingStr() << endl
      << " - create date: " << parser.dbCreateDate() << endl
      << " - elements count (first bytes): " << (int)dbHeader.elCount_first << endl
      << " - elements count (main index): " << dbHeader.elCount << endl
      << " - blocks count for element: " << dbHeader.blCount << endl
      << " - ranges count: " << dbHeader.rangesCount << endl
      << " - block ID size: " << (int)dbHeader.blIdSize << endl
      << " - region record max size : " << dbHeader.recRegionMaxSize << endl
      << " - city record max size: " << dbHeader.recCityMaxSize << endl
      << " - region catalog size: " << dbHeader.ctlgRegionSize << endl
      << " - city catalog size: " << dbHeader.ctlgCitySize << endl
      << " - country record max size: " << dbHeader.recCountryMaxSize << endl
      << " - country catalog size: " << dbHeader.ctlgCountrySize << endl
    ;

    cout  << " - format description size: " << dbHeader.frmtDescSize << endl
      << " - format description:" << endl
    ;
    for (int i = 0; i < dbHeader.frmtDesc.size(); ++i)
      cout << "   \"" << dbHeader.frmtDesc[i] << "\"" << endl;
  }
  
  cout << endl;
  
  ifstream addrsFile(params.addrsFileName);
  if (addrsFile.fail())
  {
    cerr << "Address File not open: " << strerror(errno) << endl;
    return -20;
  }
  
  vector<string> addrs;
  while (!addrsFile.eof())
  {
    string addr;
    addrsFile >> addr;
    if (addrsFile.fail())
    {
      cerr << "Addresses read failure";
      if (errno)
        cerr << ": " << strerror(errno) << endl;
      cerr << endl;
      return -21;
    }
    addrs.push_back(addr);
  }
  
  
  vector<string> CitiesWithCounties = parser.location_full(addrs);
  
  ofstream geoFile(params.geoFileName);
  if (geoFile.fail())
  {
    cerr << "Geo File not open";
      if (errno)
        cerr << ": " << strerror(errno) << endl;
    return -30;
  }
  
  string outDataString;
  for (vector<string>::iterator it = CitiesWithCounties.begin(); it != CitiesWithCounties.end(); ++it)
    outDataString.append(*it + "\r\n");
  
  geoFile << outDataString;
  if (geoFile.fail())
  {
    cerr << "Geo data write failure";
      if (errno)
        cerr << ": " << strerror(errno) << endl;
    return -30;
  }
  
  return 0;
}

#define PO_SHOW_DBINFO "db-info"

#define PO_DB "db-file"
#define PO_ADDRS "addrs-file"
#define PO_GEO "geo-file"
bool program_options_handler(Params_t * params, int argc, char** argv)
{
  po::options_description desc("General options");
  desc.add_options()
    ("help,h", "show help")
    (PO_DB",b", po::value<string>(&params->dbFileName)->default_value(""), "SxGeo DB file (ver >= 2.1)")
    (PO_ADDRS",a", po::value<string>(&params->addrsFileName)->default_value(""), "IP addresses file")
    (PO_GEO",o", po::value<string>(&params->geoFileName)->default_value(""), "Output file with Geo data")
  
    (PO_SHOW_DBINFO, "Show DB file info")
  ;
  
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  
  if (vm.count("help"))
  {
    cout << desc << endl;
    return 1;
  }
  
  params->is_showDbInfo = vm.count(PO_SHOW_DBINFO) > 0;

  return 0;
}
